package de.ffa.playerdata;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ffa.FFA;

public class PlayerStats implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
		if(!(sender instanceof Player)){
			sender.sendMessage(FFA.getMsg("NotAPlayer"));
			return true;
		}
		Player p = (Player)sender;
		if(args.length == 0){
			FFA.printStats(p, p);
			return true;
		}

		if(args.length == 1 && FFA.isOnline(sender, args[0])){
			FFA.printStats(p, Bukkit.getPlayer(args[0]));
			return true;
		}
		return false;
	}

}
