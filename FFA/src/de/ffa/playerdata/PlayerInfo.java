package de.ffa.playerdata;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

//Damit der Zugehörigkeitsort namendlicher ist.
@SerializableAs("PlayerInfo")
public class PlayerInfo implements ConfigurationSerializable{

	private String name;
	private long points;
	private int kills;
	private int deaths;
	private long timeMillis;

	PlayerInfo(String name, long points, int kills, int deaths, long timeMillis){
		this.name = name;
		this.points = points;
		this.kills = kills;
		this.deaths = deaths;
		this.timeMillis = timeMillis;
	}

	public String getName(){
		return name;
	}

	public long getPoints(){
		return points;
	}

	public int getKills(){
		return kills;
	}

	public int getDeaths(){
		return deaths;
	}

	public long getTimeMillis(){
		return timeMillis;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setPoints(long points){
		this.points = points;
	}

	public void setKills(int kills){
		this.kills = kills;
	}

	public void setDeaths(int deaths){
		this.deaths = deaths;
	}

	public void setTimeMillis(long timeMillis){
		this.timeMillis = timeMillis;
	}

	@Override
	public Map<String,Object> serialize(){
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("name", name);
		map.put("points", points);
		map.put("kills", kills);
		map.put("deaths", deaths);
		map.put("timemillis", String.valueOf(timeMillis));
		return map;
	}

	public static PlayerInfo deserialize(Map<String,Object> map){
		PlayerInfo pi = new PlayerInfo(null, 0, 0, 0, 0);
		pi.setName((String)map.get("name"));
		pi.setPoints(Long.valueOf((Integer)map.get("points")));
		pi.setKills((int)map.get("kills"));
		pi.setDeaths((int)map.get("deaths"));
		pi.setTimeMillis(Long.valueOf((String)map.get("timemillis")));
		return pi;
	}

}