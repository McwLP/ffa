package de.ffa.playerdata;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.ffa.FFA;

public class PlayerData{

	/**
	 * This methode creates the PlayerData file for a Player if not exists.
	 * @return void
	 * @throws IOException
	 **/
	public static void createPlayerData(Player p){
		File f = new File(FFA.getPl().getDataFolder() + "/playerData", p.getUniqueId() + ".yml");
		if(!f.exists()){
			try{
				f.createNewFile();
				YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
				PlayerInfo pi = new PlayerInfo(null, 0, 0, 0, 0);
				pi.setName(p.getName());
				cfg.set("playerdata.stats", pi);
				cfg.save(f);
			}catch(IOException e){
				FFA.getPl().getLogger().log(Level.WARNING, "Datei konnte nicht erstellt werden!", p.getUniqueId() + ".yml");
			}
		}
	}

	/**
	 * This methode returns the PlayerInfo of a Player and calls createPlayerData,
	 * if the File does not exists.
	 * @return
	 **/
	public static PlayerInfo getPlayerData(Player p){
		File f = new File(FFA.getPl().getDataFolder() + "/playerData", p.getUniqueId() + ".yml");
		if(!(f.exists()))
			createPlayerData(p);
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
		PlayerInfo pi = (PlayerInfo)cfg.get("playerdata.stats");
		return pi;
	}

	/**
	 * This methode calls createPlayerData, if the File does not exists and saves
	 * the File after it got edited.
	 * @return void
	 * @throws IOException
	 **/
	public static void setPlayerData(PlayerInfo pi, Player p){
		File f = new File(FFA.getPl().getDataFolder() + "/playerData", p.getUniqueId() + ".yml");
		if(!(f.exists())){
			createPlayerData(p);
		}
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
		cfg.set("playerdata.stats", pi);
		try{
			cfg.save(f);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * This methode adds the amount of points to the Player.
	 * @return void
	 **/
	public static void addPoints(Player p, long amount){
		PlayerInfo pi = getPlayerData(p);
		pi.setPoints(pi.getPoints() + amount);
		setPlayerData(pi, p);
	}

	/**
	 * This methode sets the amount of points to the Player.
	 * @return void
	 **/
	public static void setPoints(Player p, long amount){
		PlayerInfo pi = getPlayerData(p);
		pi.setPoints(amount);
		setPlayerData(pi, p);
	}

}