package de.ffa.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ffa.FFA;
import de.ffa.playerdata.PlayerData;

public class PointsCommands implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(args.length != 3){
			return false;
		}
		if(FFA.isOnline(sender, args[1])){
			Player target = Bukkit.getPlayer(args[1]);
			int amount;
			try{
				amount = Integer.valueOf(args[2]);
			}catch(Exception e){
				return false;
			}
			if(args[0].equals("set")){
				PlayerData.setPoints(target, amount);
				sender.sendMessage(
					FFA.getMsg("SetPoints").replace("%TARGET%", target.getName()).replace("%AMOUNT%", String.valueOf(amount)));
				return true;
			}else if(args[0].equals("add")){
				PlayerData.addPoints(target, amount);
				sender.sendMessage(
					FFA.getMsg("AddPoints").replace("%TARGET%", target.getName()).replace("%AMOUNT%", String.valueOf(amount)));
				return true;
			}
		}
		return true;
	}

}
