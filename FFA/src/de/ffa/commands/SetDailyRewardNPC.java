package de.ffa.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import de.ffa.FFA;

public class SetDailyRewardNPC implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			sender.sendMessage(FFA.getMsg("NotAPlayer"));
			return true;
		}
		Player p = (Player)sender;
		if(!p.isOp()){
			p.sendMessage(FFA.getMsg("NotOP"));
			return true;
		}
		if(args.length != 0){
			return false;
		}
		FFA.spawnDailyRewardNPC(p.getLocation());
		return true;
	}

}
