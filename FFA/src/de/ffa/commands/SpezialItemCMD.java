package de.ffa.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import de.ffa.FFA;

public class SpezialItemCMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player)sender;
		if(args.length == 1 && args[0].equals("del")){
			FFA.addOrDelSpezialItem(p, false, 0);
			return true;
		}
		if(args.length != 2 || !args[0].equals("add")){
			return false;
		}

		try{
			int id = Integer.parseInt(args[1]);
			FFA.addOrDelSpezialItem(p, true, id);
			return true;
		}catch(Exception e){
			p.sendMessage(FFA.getMsg("InvalidID"));
		}
		return true;
	}
}