package de.ffa.spawn;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ffa.FFA;

public class Spawn implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			sender.sendMessage(FFA.getMsg("NotAPlayer"));
			return true;
		}
		Player p = (Player)sender;
		FFA.tpToSpawn(p);
		if(!FFA.fallDMG.contains(p))
			return true;
		FFA.fallDMG.remove(p);
		return true;
	}
}