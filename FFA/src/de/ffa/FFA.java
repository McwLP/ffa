package de.ffa;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.WeakHashMap;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import de.ffa.build.BlockListener;
import de.ffa.build.Build;
import de.ffa.commands.PointsCommands;
import de.ffa.commands.SetDailyRewardNPC;
import de.ffa.commands.SpezialItemCMD;
import de.ffa.events.InteractWithDailyRewardNPC;
import de.ffa.events.ClaimRewardsEvent;
import de.ffa.events.DamageChecker;
import de.ffa.events.DamageDailyRewardNPC;
import de.ffa.events.DeathListener;
import de.ffa.events.DropItemEvent;
import de.ffa.events.FallDamageListener;
import de.ffa.events.HungerListener;
import de.ffa.events.Join;
import de.ffa.events.KitInvClickListener;
import de.ffa.events.PickUpArrowEvent;
import de.ffa.events.PlayerMoveListener;
import de.ffa.events.PlayerRightClickEvent;
import de.ffa.events.QuitEvent;
import de.ffa.events.RespawnListener;
import de.ffa.kits.Kit;
import de.ffa.kits.KitCommands;
import de.ffa.playerdata.PlayerData;
import de.ffa.playerdata.PlayerInfo;
import de.ffa.playerdata.PlayerStats;
import de.ffa.spawn.SetSpawn;
import de.ffa.spawn.Spawn;
import de.ffa.spezialitems.SpezialItems;

public class FFA extends JavaPlugin{

	public static FFA pl;

	public static FFA getPl(){
		return pl;
	}

	public static HashSet<Player> fallDMG = new HashSet<Player>();
	public static HashSet<Player> build = new HashSet<Player>();
	public static HashSet<Player> hasSelKit = new HashSet<Player>();
	public static HashSet<Player> hasCooldown = new HashSet<Player>();
	public static HashSet<Player> tmpBlockAbility = new HashSet<Player>();
	public static WeakHashMap<Player,Kit> selectedKit = new WeakHashMap<Player,Kit>();

	public static final String KIT_GUI_NAME = "Kits";
	public static final String KIT_SHOP_GUI_NAME = "Shop";
	public static final String DAILY_REWARD_NAME = "Daily Reward";

	private static int pvpHeight;
	private static int killReward;

	@Override
	public void onEnable(){
		pl = this;
		// Commands
		getConfig().options().copyDefaults(true);
		saveConfig();
		getCommand("setspawn").setExecutor(new SetSpawn());
		getCommand("spawn").setExecutor(new Spawn());
		getCommand("kit").setExecutor(new KitCommands());
		getCommand("points").setExecutor(new PointsCommands());
		getCommand("build").setExecutor(new Build());
		getCommand("stats").setExecutor(new PlayerStats());
		getCommand("setDailyRewardNPC").setExecutor(new SetDailyRewardNPC());
		getCommand("spezialItem").setExecutor(new SpezialItemCMD());
		// Events
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new Join(), this);
		pm.registerEvents(new DamageChecker(), this);
		pm.registerEvents(new RespawnListener(), this);
		pm.registerEvents(new FallDamageListener(), this);
		pm.registerEvents(new DeathListener(), this);
		pm.registerEvents(new QuitEvent(), this);
		pm.registerEvents(new BlockListener(), this);
		pm.registerEvents(new KitInvClickListener(), this);
		pm.registerEvents(new DropItemEvent(), this);
		pm.registerEvents(new PlayerRightClickEvent(), this);
		pm.registerEvents(new PlayerMoveListener(), this);
		pm.registerEvents(new HungerListener(), this);
		pm.registerEvents(new PickUpArrowEvent(), this);
		pm.registerEvents(new DamageDailyRewardNPC(), this);
		pm.registerEvents(new InteractWithDailyRewardNPC(), this);
		pm.registerEvents(new ClaimRewardsEvent(), this);
		// ConfigurationSerialization
		ConfigurationSerialization.registerClass(Kit.class, "Kit");
		ConfigurationSerialization.registerClass(PlayerInfo.class, "PlayerInfo");
		ConfigurationSerialization.registerClass(SpezialItems.class, "SpezialItems");
		// Methode zum Erzeugen von Verzeichnissen.
		createFolder("playerData", "PlayerData-Ordner wurde erstellt.");
		createFolder("kits", "Kit-Ordner wurde erstellt.");
		createFolder("spezialItems", "SpezialItems-Ordner wurde erstellt");
		// Variablen
		pvpHeight = getConfig().getInt("pvpheight");
		killReward = getConfig().getInt("killreward");
		// Spawn Entity:
		spawnDailyRewardNPC(getConfig().getSerializable("Villager", Location.class));
	}

	@Override
	public void onDisable(){
		fallDMG.clear();
		build.clear();
		hasSelKit.clear();
		hasCooldown.clear();
		tmpBlockAbility.clear();
		selectedKit.clear();
	}

	/**
	 * This methode returns the PvP-height-value, which you can edit in the
	 * config.yml. The standard-Value is 0.
	 * @return int
	 **/
	public static int getPvPHeight(){
		return pvpHeight;
	}

	/**
	 * This methode returns the killreward-value, which you can edit in the
	 * config.yml. The standard-Value is 0.
	 * @return int
	 **/
	public static int getKillReward(){
		return killReward;
	}

	/**
	 * This methode creates a File with the choosen name and broadcasts a success
	 * message.
	 * @return void
	 **/
	public static void createFolder(String name, String sucessMSG){
		File f = new File(FFA.getPl().getDataFolder(), name);
		if(!f.exists()){
			f.mkdir();
			Bukkit.broadcastMessage(sucessMSG);
		}
	}

	/**
	 * This methode returns a String if the input is valid in the Config.yml.
	 * Otherwise a error String will be returned.
	 * @return String
	 **/
	public static String getMsg(String key){
		String output = pl.getConfig().getConfigurationSection("messages").getString(key);
		if(output == null){
			return "Fehler! Kein String unter dem Namen " + key + " gefunden!";
		}
		return output;
	}

	/**
	 * This methode is opening the KitGUI for a Player and fills all existing Kits
	 * into the GUI.
	 * @return void
	 **/
	public static void openKitGUI(Player p, ArrayList<Kit> kits){
		Inventory inv = Bukkit.createInventory(null, 9 * 6, KIT_GUI_NAME);
		ItemStack placeHolder = new ItemStack(Material.STAINED_GLASS_PANE);
		for(int i = 0; i < inv.getSize(); i++){
			inv.setItem(i, placeHolder);
		}
		for(int i = 0; i < kits.size(); i++){
			Kit kit = kits.get(i);
			ItemStack previewItem = createPreviewItem(p, kit);
			inv.setItem(i, previewItem);
		}
		p.openInventory(inv);
	}

	/**
	 * This methode opens the KitShopGui for a Player. It takes 2 parameters. The
	 * Player and the Kit they Player is going to buy.
	 * @return void
	 **/
	public static void openKitShop(Player p, Kit kit){
		Inventory inv = Bukkit.createInventory(null, 9 * 3, KIT_SHOP_GUI_NAME);
		ItemStack placeHolder = new ItemStack(Material.STAINED_GLASS_PANE);
		for(int i = 0; i < inv.getSize(); i++){
			inv.setItem(i, placeHolder);
		}
		ItemStack abbrechen = new ItemStack(Material.WOOL, 1, DyeColor.RED.getWoolData());
		ItemStack kaufen = new ItemStack(Material.WOOL, 1, DyeColor.GREEN.getWoolData());
		ItemMeta imAbbrechen = abbrechen.getItemMeta();
		ItemMeta imKaufen = kaufen.getItemMeta();
		imAbbrechen.setDisplayName("§4Abbrechen");
		imKaufen.setDisplayName("§aKaufen");
		abbrechen.setItemMeta(imAbbrechen);
		kaufen.setItemMeta(imKaufen);
		inv.setItem(10, abbrechen);
		inv.setItem(16, kaufen);
		inv.setItem(13, new ItemStack(createPreviewItem(p, kit)));
		p.openInventory(inv);
	}

	/**
	 * This methode returns the edited ItemStack, which the Payer is holding in the
	 * mainHand and adds the lore, the DisplayName and the state.
	 * @return ItemStack
	 **/
	public static ItemStack createPreviewItem(Player p, Kit kit){
		ItemStack previewItem = kit.getPreview();
		ItemMeta im = previewItem.getItemMeta();
		ItemStack[] kitContent = kit.getContents();
		ArrayList<String> lore = new ArrayList<String>();
		if(selectedKit.containsKey(p) && selectedKit.get(p).getName().equals(kit.getName())){
			im.addEnchant(Enchantment.KNOCKBACK, 1, false);
		}
		if(kit.hasKit(p)){
			lore.add("Status: " + "§agekauft");
			lore.add("-------------");
		}else{
			lore.add("Status: " + "§4nicht gekauft.");
			if(PlayerData.getPlayerData(p).getPoints() >= kit.getPrice()){
				lore.add("Preis: " + "§a" + kit.getPrice());
			}else{
				lore.add("Preis: " + "§c" + kit.getPrice());
			}
			lore.add("------------------");
		}
		for(ItemStack item : kitContent){
			if(item != null){
				lore.add("- " + item.getType().toString().replace("_", " "));
			}
		}
		im.setLore(lore);
		im.setDisplayName(kit.getName());
		previewItem.setItemMeta(im);
		return previewItem;
	}

	/**
	 * This methode returns all Kits in the Kits folder. As a ArrayList of the Typ
	 * of Kit.
	 * @return ArrayList<Kit>
	 **/
	public static ArrayList<Kit> getAllKits(){
		ArrayList<Kit> kits = new ArrayList<>();
		File f = new File(FFA.getPl().getDataFolder(), "kits");
		for(File file : f.listFiles()){
			YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			Kit kit = (Kit)cfg.get(file.getName().replace(".yml", ""));
			kits.add(kit);
		}
		Collections.sort(kits);
		return kits;
	}

	/**
	 * This methode is saving the selection of the Kit for the Player in the HasMap
	 * if he ownes it and calls openKitShop methode if the Player isnt owning the
	 * selectedKit.
	 * @return void
	 **/
	// Wenn der Spieler Kit hat wird ausgewählt, ansonsten wird Shop geöffnet.
	public static void hasKit(Player p, String clickedItem){
		ArrayList<Kit> kits = getAllKits();
		for(Kit kit : kits){
			if(kit.getName().equals(clickedItem)){
				if(kit.hasKit(p)){
					p.sendMessage(getMsg("SelKit").replace("%KIT%", kit.getName()));
					selectedKit.put(p, kit);
					return;
				}
				openKitShop(p, kit);
				return;
			}
		}
	}

	/**
	 * This methode gives the Player a kit if he is in the HashMap and teleports him
	 * back to the Spawn if not.
	 * @return void
	 **/
	public static void giveKit(Player p){
		if(!selectedKit.containsKey(p)){
			tpToSpawn(p);
			p.sendMessage(getMsg("NoKitSel"));
			return;
		}
		p.getInventory().setContents(selectedKit.get(p).getContents());
	}

	/**
	 * This methode returns true, if the Player is online and false if not. It also
	 * prints a message, if the Player isn´t online.
	 * @return boolean
	 **/
	public static boolean isOnline(CommandSender sender, String name){
		if(Bukkit.getPlayer(name) == null){
			sender.sendMessage(getMsg("PlayerNotOnline").replace("%PLAYER%", name));
			return false;
		}
		return true;
	}

	/**
	 * This Methode prints the PlayerStats of the target Player for the requiring
	 * Player.
	 * @return void
	 **/
	public static void printStats(Player p, Player target){
		PlayerInfo pi = PlayerData.getPlayerData(target);
		p.sendMessage("§aPoints: " + pi.getPoints());
		p.sendMessage("§aKills: " + pi.getKills());
		p.sendMessage("§aDeaths: " + pi.getDeaths());
		p.sendMessage("§aK/D: " + (Math.round((double)pi.getKills() / (double)pi.getDeaths() * 100)) / 100.00);
	}

	/**
	 * This methode teleports the Player to the Spawn, if the SpawnLocation exists.
	 * It also sets the KitGUI-Item into the Inventory of the Player and removes the
	 * Player from the fallDMG HashSet.
	 * @return void
	 **/
	public static void tpToSpawn(Player p){
		if(FFA.pl.getConfig().getString("Spawn") == null){
			p.sendMessage(getMsg("SpawnNotFound"));
			return;
		}else{
			p.getInventory().clear();
			ItemStack kits = new ItemStack(Material.CHEST);
			ItemMeta im = kits.getItemMeta();
			im.setDisplayName("§aKits");
			kits.setItemMeta(im);
			p.getInventory().setItem(4, kits);
			p.getInventory().setHeldItemSlot(4);
			p.teleport((Location)FFA.pl.getConfig().get("Spawn"));
			if(hasSelKit.contains(p))
				hasSelKit.remove(p);
			fallDMG.remove(p);
		}

	}

	/**
	 * This methode returns true, if the Player has the Kit and false when not.
	 * @return void
	 **/
	public static void spawnDailyRewardNPC(Location loc){
		removeDailyRewardNPC();
		Location configLoc = FFA.getPl().getConfig().getSerializable("Villager", Location.class);
		if(configLoc == null)
			return;
		ArmorStand arm = (ArmorStand)loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
		Villager vil = (Villager)loc.getWorld().spawnEntity(loc, EntityType.VILLAGER);
		// Armorstand
		arm.setPassenger(vil);
		arm.setVisible(false);
		arm.setMarker(true);
		arm.setSmall(true);
		arm.setVelocity(new Vector(0, 0, 0));
		arm.setGravity(false);
		// Villager
		vil.setAdult();
		vil.setAI(false);
		vil.setCustomName(FFA.DAILY_REWARD_NAME);
		vil.setCustomNameVisible(true);
		vil.setGlowing(true);
		vil.setProfession(Profession.LIBRARIAN);

		if(loc == configLoc)
			return;
		FFA.getPl().getConfig().set("Villager", loc);
		FFA.getPl().saveConfig();

	}

	/**
	 * This methode returns true, if the Player has the Kit and false when not.
	 * @return void
	 **/
	public static void removeDailyRewardNPC(){
		Location loc = FFA.getPl().getConfig().getSerializable("Villager", Location.class);
		if(loc != null){
			for(Villager v : loc.getWorld().getEntitiesByClass(Villager.class)){
				if(v != null && v.getCustomName().equals(DAILY_REWARD_NAME)){
					v.getVehicle().remove();
					v.remove();
				}
			}
		}
	}

	/**
	 * This Methode is used to add or delete a ItemStack from the ConfigList of the
	 * spezial items
	 * @return void
	 * @throws IOException
	 **/
	public static void addOrDelSpezialItem(Player p, boolean addOrDel, int id){
		ItemStack item = p.getInventory().getItemInMainHand();
		if(item == null || item.getItemMeta().getDisplayName() == null){
			p.sendMessage(getMsg("NoItemInHand"));
			return;
		}
		File f = new File(pl.getDataFolder() + "/spezialItems", item.getItemMeta().getDisplayName() + ".yml");
		if(addOrDel){
			try{
				f.createNewFile();
				YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
				SpezialItems sI = new SpezialItems(item, id);
				cfg.set(item.getItemMeta().getDisplayName(), sI);
				cfg.save(f);
			}catch(IOException e){
				e.printStackTrace();
			}
			p.sendMessage(getMsg("ItemAdded"));
		}else if(!f.exists())
			p.sendMessage(getMsg("ItemNotExisting"));
		else{
			f.delete();
			p.sendMessage(getMsg("ItemRemoved"));
		}
	}

	/**
	 * This methode returns a ArrayList of SpezialItems.
	 * @return ArrayList<SpezialItems>
	 **/
	public static ArrayList<SpezialItems> getAllSpezialItems(){
		ArrayList<SpezialItems> sI = new ArrayList<>();
		File f = new File(FFA.getPl().getDataFolder(), "spezialItems");
		for(File file : f.listFiles()){
			YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			SpezialItems item = (SpezialItems)cfg.get(file.getName().replace(".yml", ""));
			sI.add(item);
		}
		return sI;
	}

	/**
	 * This methode places a Block one Blocks under the Player if the Block is air
	 * and removes it after two Secounds.
	 * @return void
	 **/
	public static void tmpBlock(Player p){
		Location loc = p.getLocation();
		loc.setY(loc.getY() - 1);
		Block before = loc.getBlock();
		if(before.getType() != Material.AIR)
			return;
		before.setType(Material.STAINED_GLASS);
		new BukkitRunnable(){
			@Override
			public void run(){
				before.setType(Material.AIR);
			}
		}.runTaskLater(FFA.getPl(), 40);
	}
}