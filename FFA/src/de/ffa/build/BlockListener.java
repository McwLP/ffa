package de.ffa.build;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import de.ffa.FFA;

public class BlockListener implements Listener{

	@EventHandler
	public void onBreake(BlockBreakEvent evt){
		Player p = evt.getPlayer();
		if(!FFA.build.contains(p)){
			p.sendMessage(FFA.getMsg("CantPlaceBlocks"));
			evt.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlaceBlock(BlockPlaceEvent evt){
		Player p = evt.getPlayer();
		if(!FFA.build.contains(p)){
			p.sendMessage(FFA.getMsg("CantBreakBlock"));
			evt.setCancelled(true);
		}
	}
}
