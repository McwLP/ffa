package de.ffa.build;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.ffa.FFA;

public class Build implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
		if(!(sender instanceof Player)){
			sender.sendMessage(FFA.getMsg("NotAPlayer"));
		}else{
			Player p = (Player)sender;
			if(!FFA.build.contains(p)){
				FFA.build.add(p);
				p.sendMessage(FFA.getMsg("BuildModeON"));
			}else{
				FFA.build.remove(p);
				if(p.getLocation().getBlockY() > FFA.getPvPHeight())
					FFA.tpToSpawn(p);
				p.sendMessage(FFA.getMsg("BuildModeOFF"));
			}
		}
		return false;
	}

}
