package de.ffa.events;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import de.ffa.FFA;

public class DamageChecker implements Listener{

	@EventHandler
	public void onDamge(EntityDamageByEntityEvent evt){
		if(evt.getEntityType() != EntityType.PLAYER)
			return;
		if(evt.getEntity().getLocation().getBlockY() >= FFA.getPvPHeight())
			evt.setCancelled(true);
	}
}
