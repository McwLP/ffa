package de.ffa.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import de.ffa.FFA;

public class PlayerMoveListener implements Listener{

	@EventHandler
	public void onMove(PlayerMoveEvent evt){
		Player p = evt.getPlayer();
		if(p.getLocation().getBlockY() > FFA.getPvPHeight())
			return;
		if(!FFA.hasSelKit.contains(p)){
			p.getOpenInventory().close();
			FFA.hasSelKit.add(p);
			FFA.giveKit(p);
		}
		if(!FFA.tmpBlockAbility.contains(p))
			return;
		FFA.tmpBlock(p);
	}
}