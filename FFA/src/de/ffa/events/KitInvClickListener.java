package de.ffa.events;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import de.ffa.FFA;
import de.ffa.kits.Kit;
import de.ffa.playerdata.PlayerData;
import de.ffa.playerdata.PlayerInfo;

public class KitInvClickListener implements Listener{

	@EventHandler
	public void onInventoryClickKitGUI(InventoryClickEvent evt){
		if(evt.getClickedInventory() == null || !evt.getInventory().getTitle().equals(FFA.KIT_GUI_NAME)){
			return;
		}
		ItemStack clicked = evt.getCurrentItem();
		evt.setCancelled(true);
		for(Kit kit : FFA.getAllKits()){
			if(kit.getName().equals(clicked.getItemMeta().getDisplayName())){
				Player p = (Player)evt.getWhoClicked();
				if(!kit.getPlayers().contains(p.getUniqueId())){
					p.getOpenInventory().close();
					FFA.openKitShop(p, kit);
					return;
				}
				FFA.selectedKit.put((Player)p, kit);
				FFA.openKitGUI(p, FFA.getAllKits());
				p.sendMessage(FFA.getMsg("SelKit").replace("%KIT%", kit.getName()));
				return;
			}
		}
	}

	@EventHandler
	public void onInventoryCickKitShop(InventoryClickEvent evt){
		if(evt.getClickedInventory() == null || !evt.getInventory().getTitle().equals(FFA.KIT_SHOP_GUI_NAME)){
			return;
		}
		evt.setCancelled(true);
		Player p = (Player)evt.getWhoClicked();
		if(evt.getSlot() == 16){
			PlayerInfo pi = PlayerData.getPlayerData(p);
			String kitGuess = evt.getClickedInventory().getItem(13).getItemMeta().getDisplayName();
			File f = new File(FFA.getPl().getDataFolder() + "/kits", kitGuess + ".yml");
			YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
			Kit kit = (Kit)cfg.get(kitGuess);
			if(pi.getPoints() < kit.getPrice()){
				p.sendMessage(FFA.getMsg("NotEnoughPoints").replace("%KIT%", kitGuess));
				return;
			}
			kit.addPlayer(p.getUniqueId());
			cfg.set(kitGuess, kit);
			try{
				cfg.save(f);
			}catch(IOException e){
				e.printStackTrace();
			}
			PlayerData.addPoints(p, -kit.getPrice());
			p.sendMessage(FFA.getMsg("KitBought").replace("%KIT%", kitGuess));
			p.getOpenInventory().close();

		}else if(evt.getSlot() == 10){
			p.sendMessage(FFA.getMsg("KitPaymentCancled"));
			p.getOpenInventory().close();
			return;
		}
	}
}
