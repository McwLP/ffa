package de.ffa.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import de.ffa.FFA;

public class DamageDailyRewardNPC implements Listener{

	@EventHandler
	public void onDamageEntity(EntityDamageEvent evt){
		String customName = evt.getEntity().getCustomName();
		if(customName == null || !customName.equals(FFA.DAILY_REWARD_NAME))
			return;
		evt.setCancelled(true);
	}

}
