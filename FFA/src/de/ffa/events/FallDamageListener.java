package de.ffa.events;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import de.ffa.FFA;

public class FallDamageListener implements Listener{

	@EventHandler
	public void onDeath(EntityDamageEvent evt){
		if(evt.getEntityType() != EntityType.PLAYER){
			return;
		}
		Player p = (Player)evt.getEntity();
		if(p.getLocation().getY() > FFA.getPvPHeight()) {
			evt.setCancelled(true);
			return;
		}
		if(!FFA.fallDMG.contains(p)){
			evt.setCancelled(true);
			FFA.fallDMG.add(p);
		}
	}
}