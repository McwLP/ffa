package de.ffa.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.ffa.FFA;

public class QuitEvent implements Listener{

	@EventHandler
	public void onQuit(PlayerQuitEvent evt){
		Player p = evt.getPlayer();
		evt.setQuitMessage(FFA.getMsg("Quit").replace("%PLAYER%", p.getName()));
		if(!FFA.fallDMG.contains(p)){
			return;
		}
		FFA.fallDMG.remove(p);
	}
}