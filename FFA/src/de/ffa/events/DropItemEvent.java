package de.ffa.events;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import de.ffa.FFA;

public class DropItemEvent implements Listener{

	@EventHandler
	public void onItemDop(PlayerDropItemEvent evt){
		Player p = evt.getPlayer();
		if(p.getGameMode() == GameMode.CREATIVE)
			return;
		if(evt.getItemDrop().getItemStack().getType() == Material.CHEST){
			evt.setCancelled(true);
			FFA.openKitGUI(p, FFA.getAllKits());
		}
		evt.setCancelled(true);
	}
}