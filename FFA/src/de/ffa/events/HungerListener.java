package de.ffa.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import de.ffa.FFA;

public class HungerListener implements Listener{

	@EventHandler
	public void onHunger(FoodLevelChangeEvent evt){
		if(!(evt.getEntity() instanceof Player))
			return;
		Player p = (Player)evt.getEntity();
		if(p.getLocation().getY() > FFA.getPvPHeight())
			evt.setCancelled(true);
	}

}
