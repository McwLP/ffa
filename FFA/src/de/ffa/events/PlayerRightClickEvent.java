package de.ffa.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import de.ffa.FFA;
import de.ffa.spezialitems.SpezialItems;

public class PlayerRightClickEvent implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent evt){
		Player p = evt.getPlayer();
		ItemStack item = p.getInventory().getItemInMainHand();
		if(item.getType() == Material.CHEST){
			FFA.openKitGUI(p, FFA.getAllKits());
			evt.setCancelled(true);
		}
		for(SpezialItems s : FFA.getAllSpezialItems()){
			if(s.equals(new SpezialItems(item, 0))){
				s.ability(p);
				evt.setCancelled(true);
				return;
			}
		}
	}

}