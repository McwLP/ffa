package de.ffa.events;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import de.ffa.FFA;
import de.ffa.playerdata.PlayerData;
import de.ffa.playerdata.PlayerInfo;

public class ClaimRewardsEvent implements Listener{

	@EventHandler
	public void onClickInventory(InventoryClickEvent evt){
		if(evt.getClickedInventory() == null || !evt.getClickedInventory().getTitle().equals(FFA.DAILY_REWARD_NAME))
			return;
		evt.setCancelled(true);
		if(!(evt.getCurrentItem().getType() == Material.ENDER_CHEST))
			return;
		Player p = (Player)evt.getWhoClicked();
		p.closeInventory();
		PlayerInfo pi = PlayerData.getPlayerData(p);
		pi.setTimeMillis(System.currentTimeMillis());
		Random random = new Random();
		int reward = random.nextInt(100) * FFA.getKillReward();
		pi.setPoints(pi.getPoints() + reward);
		PlayerData.setPlayerData(pi, p);
		p.sendMessage(FFA.getMsg("GetPoints").replace("%AMOUNT%", String.valueOf(reward)));
	}

}
