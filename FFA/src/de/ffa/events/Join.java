package de.ffa.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import de.ffa.FFA;
import de.ffa.playerdata.PlayerData;

public class Join implements Listener{

	@EventHandler
	public void onJoin(PlayerJoinEvent evt){
		Player p = evt.getPlayer();
		if(FFA.getPl().getConfig().getString("Spawn") == null)
			return;
		FFA.tpToSpawn(p);
		PlayerData.createPlayerData(p);
		evt.setJoinMessage(FFA.getMsg("Join").replace("%PLAYER%", p.getName()));
	}

}