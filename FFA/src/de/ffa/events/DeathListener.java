package de.ffa.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import de.ffa.FFA;
import de.ffa.playerdata.PlayerData;
import de.ffa.playerdata.PlayerInfo;

public class DeathListener implements Listener{

	@EventHandler
	public void onDeath(PlayerDeathEvent evt){
		Player p = evt.getEntity().getPlayer();
		if(!FFA.fallDMG.contains(p))
			return;
		if(FFA.hasCooldown.contains(p))
			FFA.hasCooldown.remove(p);
		PlayerInfo pi = PlayerData.getPlayerData(p);
		pi.setDeaths(pi.getDeaths() + 1);
		PlayerData.setPlayerData(pi, p);
		FFA.fallDMG.remove(p);
		evt.getDrops().clear();
		if(evt.getEntity().getKiller() instanceof Player){
			int killReward = FFA.getKillReward();
			Player k = p.getKiller();
			pi = PlayerData.getPlayerData(k);
			pi.setKills(pi.getKills() + 1);
			pi.setPoints(pi.getPoints() + killReward);
			PlayerData.setPlayerData(pi, k);
			k.sendMessage(FFA.getMsg("KillReward").replace("%AMOUNT%", String.valueOf(killReward)));
			evt.setDeathMessage(FFA.getMsg("KilledByPlayer").replace("%PLAYER%", p.getName()).replace("%KILLER%", k.getName()));
		}else{
			evt.setDeathMessage(FFA.getMsg("SuicideDeath").replace("%PLAYER%", p.getName()));
		}
	}

}
