package de.ffa.events;

import java.text.DecimalFormat;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.ffa.FFA;
import de.ffa.playerdata.PlayerData;
import de.ffa.playerdata.PlayerInfo;

public class InteractWithDailyRewardNPC implements Listener{

	@EventHandler
	public void onInteractWithVillager(PlayerInteractEntityEvent evt){
		if(evt.getRightClicked().getCustomName() == null
			|| !evt.getRightClicked().getCustomName().equals(FFA.DAILY_REWARD_NAME)){
			return;
		}
		evt.setCancelled(true);
		Player p = evt.getPlayer();
		PlayerInfo pi = PlayerData.getPlayerData(p);
		long timeLeft = System.currentTimeMillis() - pi.getTimeMillis();
		double tmp = 86400000 - timeLeft;
		if(timeLeft < 86400000){
			DecimalFormat df = new DecimalFormat("0.00");
			p.sendMessage(FFA.getMsg("DailyRewardTime").replace("%TIMER%", df.format(tmp / 1000 / 60 / 60)));
			return;
		}
		Inventory inv = Bukkit.createInventory(p, 6 * 9, FFA.DAILY_REWARD_NAME);
		for(int i = 0; i < inv.getSize(); i++){
			inv.setItem(i, new ItemStack(Material.STAINED_GLASS_PANE));
		}
		Random random = new Random();
		ItemStack reward = new ItemStack(Material.ENDER_CHEST);
		for(int i = 0; i < 5; i++){
			int index = random.nextInt(inv.getSize() - 1);
			if(inv.getItem(index).getType() != Material.ENDER_CHEST){
				inv.setItem(index, reward);
				continue;
			}
			i--;
		}
		p.openInventory(inv);
	}

}
