package de.ffa.events;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import de.ffa.FFA;

public class RespawnListener implements Listener{

	@EventHandler
	public void onRespawn(PlayerRespawnEvent evt){
		if(!FFA.pl.getConfig().contains("Spawn"))
			return;
		evt.setRespawnLocation((Location)FFA.pl.getConfig().get("Spawn"));
		FFA.tpToSpawn(evt.getPlayer());
	}
}