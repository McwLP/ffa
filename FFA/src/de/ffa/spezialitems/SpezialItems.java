package de.ffa.spezialitems;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import de.ffa.FFA;

@SerializableAs("SpezialItems")
public class SpezialItems implements ConfigurationSerializable{

	private ItemStack item;
	private int id;

	public SpezialItems(ItemStack item, int id){
		this.item = item;
		this.id = id;
	}

	public ItemStack getItem(){
		return item;
	}

	public void setItem(ItemStack item){
		this.item = item;
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

	public void ability(Player p){
		switch(id){
		case 0:
			//Spieler kann Fliegen
			if(!FFA.hasCooldown.contains(p)){
				FFA.hasCooldown.add(p);
				p.setAllowFlight(true);
				new BukkitRunnable(){

					@Override
					public void run(){
						p.setAllowFlight(false);
					}
				}.runTaskLater(FFA.getPl(), 200);
				new BukkitRunnable(){

					@Override
					public void run(){
						FFA.hasCooldown.remove(p);
					}
				}.runTaskLater(FFA.getPl(), 400);
			}
			break;
		case 1:
			//Spieler bekommt Speed
			if(!FFA.hasCooldown.contains(p)){
				FFA.hasCooldown.add(p);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 5));
				new BukkitRunnable(){

					@Override
					public void run(){
						FFA.hasCooldown.remove(p);
					}
				}.runTaskLater(FFA.getPl(), 200);
			}
			break;
		case 2:
			//Unter dem Spieler werden Blöcke beim laufen platziert
			if(!FFA.hasCooldown.contains(p)){
				FFA.hasCooldown.add(p);
				FFA.tmpBlockAbility.add(p);

				new BukkitRunnable(){

					@Override
					public void run(){
						FFA.tmpBlockAbility.remove(p);
					}
				}.runTaskLater(FFA.getPl(), 100);

				new BukkitRunnable(){

					@Override
					public void run(){
						FFA.hasCooldown.remove(p);
					}
				}.runTaskLater(FFA.getPl(), 200);
			}
			break;
		default:
			p.sendMessage(FFA.getMsg("IDNotFound"));
			break;
		}

	}

	@Override
	public Map<String,Object> serialize(){
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("item", item);
		map.put("id", id);
		return map;
	}

	public static SpezialItems deserialize(Map<String,Object> map){
		SpezialItems item = new SpezialItems(null, 0);
		item.setItem((ItemStack)map.get("item"));
		item.setId((int)map.get("id"));
		return item;
	}

	/**
	 * This methode Overrides the equals Methode to compare Objects with it.
	 * @return boolean
	 **/
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof SpezialItems))
			return false;
		SpezialItems sI = (SpezialItems)obj;
		return sI.getItem().equals(this.item);
	}
}