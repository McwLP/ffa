package de.ffa.kits;

import java.io.File;
import java.io.IOException;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.ffa.FFA;

public class KitCommands implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			sender.sendMessage(FFA.getMsg("NotAPlayer"));
			return true;
		}
		Player p = (Player)sender;
		if(args.length == 0){
			if(p.getLocation().getBlockY() <= FFA.getPvPHeight()){
				p.sendMessage(FFA.getMsg("NotOnSpawn"));
				return true;
			}
			FFA.openKitGUI(p, FFA.getAllKits());
			return true;
		}
		if(!p.isOp()){
			p.sendMessage(FFA.getMsg("NotOP"));
			return true;
		}
		// Falls der Kits-Ordner während der Laufzeit gelöscht wird, wird er
		// neuerstellt.
		File f = new File(FFA.getPl().getDataFolder(), "kits");
		if(args.length == 3 && args[0].equals("create")){
			if(!(f.exists())){
				FFA.createFolder("kits", "Kit-Ordner wurde erstellt.");
				return true;
			}
			if(FFA.getAllKits().size() >= 54){
				p.sendMessage(FFA.getMsg("MaxKitsInGUI"));
				return true;
			}
			f = new File(FFA.getPl().getDataFolder() + "/kits", args[1] + ".yml");
			if(!(f.exists())){
				try{
					f.createNewFile();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
			ItemStack itemInMainHand = p.getInventory().getItemInMainHand();
			if(itemInMainHand == null || itemInMainHand.getType() == Material.AIR){
				p.sendMessage(FFA.getMsg("NoItemInHand"));
				return true;
			}
			long price;
			String priceString = args[2];
			try{
				price = Long.valueOf(args[2]);
				Long.parseLong(priceString);
			}catch(NumberFormatException e){
				return false;
			}
			Kit kit = new Kit(args[1], price, p.getInventory().getContents(), itemInMainHand);
			cfg.set(kit.getName(), kit);
			p.sendMessage(FFA.getMsg("KitCreated").replace("%KIT%", kit.getName()));
			try{
				cfg.save(f);
			}catch(IOException e){
				e.printStackTrace();
			}
		}else if(args.length == 2 && args[0].equals("del")){
			f = new File(FFA.getPl().getDataFolder() + "/kits", args[1] + ".yml");
			if(!(f.exists())){
				p.sendMessage(FFA.getMsg("KitNotFound").replace("%KIT%", args[1]));
				return true;
			}
			f.delete();
			p.sendMessage(FFA.getMsg("KitDeleted").replace("%KIT%", args[1]));
		}else{
			return false;
		}
		return true;
	}
}
