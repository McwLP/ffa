package de.ffa.kits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@SerializableAs("Kit")
public class Kit implements ConfigurationSerializable, Comparable<Object>{

	private String name;
	private long price;
	private ItemStack[] contents;
	private ItemStack preview;
	private ArrayList<UUID> players = new ArrayList<UUID>();

	public Kit(String name, long price, ItemStack[] itemStacks, ItemStack preview){
		this.name = name;
		this.price = price;
		this.contents = itemStacks;
		this.preview = preview;

	}

	public String getName(){
		return name;
	}

	public long getPrice(){
		return price;
	}

	public ItemStack[] getContents(){
		return contents;
	}

	public ItemStack getPreview(){
		return preview;
	}

	public ArrayList<UUID> getPlayers(){
		return players;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setPrice(long price){
		this.price = price;
	}

	public void setContents(ItemStack[] contents){
		this.contents = contents;
	}

	public void setPreview(ItemStack preview){
		this.preview = preview;
	}

	public void setPlayers(ArrayList<UUID> players){
		this.players = players;
	}

	/**
	 * This methode adds a Player to the Kitobject.
	 * @return void
	 **/
	public void addPlayer(UUID uuid){
		this.players.add(uuid);
	}

	/**
	 * This methode returns true, if the Player has the Kit and false when not.
	 * @return boolean
	 **/
	public boolean hasKit(Player p){
		return players.contains(p.getUniqueId());
	}
	
	@Override
	public int compareTo(Object o) {
		if(!(o instanceof Kit))
			return 0;
		Kit compare = (Kit) o;
		if(this.price > compare.getPrice())
			return 1;
		if(this.price < compare.getPrice())
			return -1;
		return 0;
	}

	@Override
	public Map<String,Object> serialize(){
		HashMap<String,Object> r = new HashMap<>();
		r.put("name", name);
		r.put("price", price);
		r.put("contents", contents);
		r.put("preview", preview);
		ArrayList<String> idStrings = new ArrayList<String>();
		for(UUID id : players){
			idStrings.add(id.toString());
		}
		r.put("players", idStrings);
		return r;
	}

	public static Kit deserialize(Map<String,Object> map){
		Kit kit = new Kit(null, 0, null, null);
		kit.setName((String)map.get("name"));
		// valueOf, damit beim Einlsesen der eingelesene Integer zu einem Long wird.
		kit.setPrice(Long.valueOf((int)map.get("price")));
		ItemStack[] contents = new ItemStack[41];
		// Object raw = map.get("contents");
		ArrayList<ItemStack> items = (ArrayList<ItemStack>)map.get("contents");
		for(int i = 0; i < items.size(); i++){
			contents[i] = items.get(i);
		}
		kit.setContents(contents);
		kit.setPreview((ItemStack)map.get("preview"));
		ArrayList<UUID> uuids = new ArrayList<UUID>();
		for(String string : (ArrayList<String>)map.get("players")){
			uuids.add(UUID.fromString(string));
		}
		kit.setPlayers(uuids);
		return kit;
	}
}